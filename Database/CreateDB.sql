/*
Created		13/08/2014
Modified		19/02/2015
Project		ADUG Autumn Symposium 2015
Model		WorkLog
Company		The Continuity Group Pty Ltd
Author		Lachlan & Mathias
Version		1.00
Database		Firebird 
*/


Create Domain DPK As Integer;
Create Domain DName As Varchar(20);
Create Domain DDateOnly As Date;
Create Domain DVersionNo As Integer;
Create Domain DHours As Double precision;
Create Domain DTimestamp As Timestamp;


Create Table Employee  (
	EmployeeID DPK NOT NULL,
	FirstName DName NOT NULL,
	LastName DName NOT NULL,
	UpdNo DVersionNo NOT NULL,
Constraint pk_Employee Primary Key (EmployeeID)
);

Create Table Project  (
	ProjectID DPK NOT NULL,
	Name DName NOT NULL,
	UpdNo DVersionNo NOT NULL,
Constraint pk_Project Primary Key (ProjectID)
);

Create Table ProjectWork  (
	ProjectWorkID DPK NOT NULL,
	EmployeeID DPK NOT NULL,
	ProjectID DPK NOT NULL,
	StartDate DDateOnly NOT NULL,
	EndDate DDateOnly,
	Hours DHours,
	ProcessedStmp DTimestamp,
	UpdNo DVersionNo NOT NULL,
Constraint pk_ProjectWork Primary Key (ProjectWorkID)
);

Create Table DBVersion  (
	DBVersionID Integer NOT NULL,
	Comment Varchar(150) NOT NULL,
	CreateDate DDateOnly NOT NULL,
	UpgradeStmp DTimestamp,
Constraint pk_DBVersion Primary Key (DBVersionID)
);


Alter Table Project add Constraint AK_Project UNIQUE (Name);


Create Index IX_Employee_LastName  ON Employee (LastName);
Create Index IX_Employee_FirstName  ON Employee (FirstName);
Create Index IX_ProjectWork_StartDate  ON ProjectWork (StartDate);


Alter Table ProjectWork add Constraint FK_Employee_ProjectWork Foreign Key (EmployeeID) references Employee (EmployeeID) on update no action on delete no action ;
Alter Table ProjectWork add Constraint FK_Project_ProjectWork Foreign Key (ProjectID) references Project (ProjectID) on update no action on delete no action ;


Create Generator GenEmployeeID;

Create Generator GenProjectID;

Create Generator GenProjectWorkID;


Commit Work;

Insert Into DBVersion (DBVersionID, Comment, CreateDate, UpgradeStmp) Values (1, 'First database version 1.00.00', '2015-02-19', Current_Timestamp);

Commit;


